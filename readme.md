# Umb Six

A simple manually installed copy of Umbraco Version 6. No Visual Studio management or directory nesting.

## Site Setup

* Checkout the master branch
* Build the solution inside Visual Studio
* Run PowerShell as an administrator. Run 'Set-ExecutionPolicy RemoteSigned'
* Run the '.\install.ps1' PowerShell script. This will setup IIS, disk permissions and update the hosts file
* Navigate to [http://umbsix.local/](http://umbsix.local/)

_Note: If you run PowerShell scripts in windows 8 you'll need to set PowerShell to version 2 by opening PowerShell (run as admin) then executing 'powershell -version 2.0'_

## Umbraco 

### URLs

*  [http://umbsix.local/](http://umbsix.local/) - homepage.
*  [http://umbsix.local/umbraco/](http://umbsix.local/umbraco/) - CMS Backoffice.

### CMS User Account
* User: Admin           
* Pass: admin